# TwitchWatch
A library created to keep track of a list of favorite broadcasters on
[Twitch.tv](https://twitch.tv/). Has a process running in the background
that can be queried at will, but will not spam Twitch with requests.

Also exports library functions to facilitate on-demand requests, if need be.

## Installation

  1. Add `twitch_watch` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:twitch_watch, git: "https://gitlab.com/gonz/twitch_watch.git"}]
    end
    ```

  2. Ensure `twitch_watch` is started before your application:

    ```elixir
    def application do
      [applications: [:twitch_watch]]
    end
    ```
### Config
The config file should be placed in `priv/config.exs`.

It should contain something like the following:

  ```elixir
  config :twitch_watch,
    favorites: ~w(arteezy esl_csgo beyondthesummit attackerdota),
    sleep_time: 300
  ```
  
The `sleep_time` config variable is the amount of time for the StreamWatcher
process to wait between refreshing of streams, and is expressed in seconds.

#### The StreamWatcher process
If you do not want the StreamWatcher process to run, specify the following
in your config:

  ```elixir
  config :twitch_watch,
    ...
    run_stream_watcher: false
  ```
    
If you don't specify this the process will run automatically.
