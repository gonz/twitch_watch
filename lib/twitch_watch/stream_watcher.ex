defmodule TwitchWatch.StreamWatcher do
  require Logger

  use GenServer

  def start_link(favorites, sleep_time) do
    GenServer.start_link(__MODULE__, [favorites, sleep_time], name: __MODULE__)
  end

  def favorites(pid \\ __MODULE__), do: GenServer.call(pid, :favorites)

  def online_streams(pid \\ __MODULE__), do: GenServer.call(pid, :online_streams)

  def handle_call(:favorites, _from, %{favorites: favorites} = state) do
    {:reply, favorites, state}
  end

  def handle_call(:online_streams, _from, %{online: online_streams} = state) do
    {:reply, online_streams, state}
  end

  def handle_info(:refresh, %{favorites: favorites,
                              sleep_time: seconds} = state) do
    Logger.debug "refreshing streams"
    Process.send_after self, :refresh, seconds * 1000
    {:noreply, %{state | online: TwitchWatch.Streams.get_online(favorites)}}
  end

  def init([favorites, sleep_time]) do
    send self, :refresh
    {:ok, %{favorites: favorites, online: [], sleep_time: sleep_time}}
  end
end
