defmodule TwitchWatch.API do
  @moduledoc"""
  Exposes the top-level functions for the API and is intended to use the plumbing
  in the lower levels of the API namespace.
  """

  alias TwitchWatch.API.Stream
  alias TwitchWatch.API.Channel
  alias TwitchWatch.API.Preview

  @base_url "https://api.twitch.tv/kraken/"

  # This is the Twitch Client-ID they use for their player, apparently.
  # It may or may not be a terrible idea to use it here like this.
  @default_headers [{"Client-ID", "jzkbprff40iqj646a697cyrvl0zt2m6"}]

  @doc"""
  Given a list of strings representing stream names (usernames, think "esl_csgo")
  will return the subset of those that are currently broadcasting, in the form of
  a list of %Stream{}.
  """
  def streams_online(streams) do
    url = @base_url <> "streams" <> make_param("channel", streams)
    %HTTPoison.Response{body: body} = HTTPoison.get! url, @default_headers
    %{"streams" => online_streams} =
      Poison.decode! body,
      as: %{"streams" => [%Stream{channel: %Channel{}, preview: %Preview{}}]}
    online_streams
  end

  defp make_param(param, data), do: "?#{param}=" <> Enum.join(data, ",")
end
