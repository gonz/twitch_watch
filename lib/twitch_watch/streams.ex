defmodule TwitchWatch.Streams do
  def get_online(streams) do
    TwitchWatch.API.streams_online(streams)
  end
end
