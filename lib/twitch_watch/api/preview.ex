defmodule TwitchWatch.API.Preview do
  @derive [Poison.Encoder]
  defstruct [:large, :medium, :small, :template]
end
