defmodule TwitchWatch.API.Stream do
  @derive [Poison.Encoder]
  defstruct [:channel, :preview]
end
