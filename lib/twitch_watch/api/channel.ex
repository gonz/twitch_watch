defmodule TwitchWatch.API.Channel do
  @derive [Poison.Encoder]
  defstruct [:name, :url, :status, :game, :logo]
end
