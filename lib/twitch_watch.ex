defmodule TwitchWatch do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = []

    favorites = Application.fetch_env! :twitch_watch, :favorites
    sleep_time = Application.get_env :twitch_watch, :sleep_time, 300
    # Add StreamWatcher if :run_stream_watcher key is true or not defined
    run_watcher = Application.get_env(:twitch_watch, :run_stream_watcher, true)
    children = children ++ if run_watcher do
      [worker(TwitchWatch.StreamWatcher, [favorites, sleep_time]) | children]
    else
      children
    end

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TwitchWatch.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
